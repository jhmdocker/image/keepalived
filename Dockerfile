ARG ALPINE_VERSION=latest

FROM alpine:$ALPINE_VERSION

ARG ALPINE_VERSION=ND
ARG KEEPALIVED_VERSION=ND

ENV CONFIG_FILE=/etc/keepalived/config.yaml \
    ALPINE_VERSION=${ALPINE_VERSION} \
    KEEPALIVED_VERSION=${KEEPALIVED_VERSION}

ENV KEEPALIVED_OPTS="-P -n -l -f /etc/keepalived/keepalived.conf"

RUN echo ALPINE_VERSION=${ALPINE_VERSION} && \
    echo KEEPALIVED_VERSION=${KEEPALIVED_VERSION} && \
    apk add --no-cache keepalived=${KEEPALIVED_VERSION} curl python3 py3-yaml py3-netifaces

COPY rootfs /

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]

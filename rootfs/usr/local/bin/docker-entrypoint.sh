#!/bin/sh -eu

if [ ! -e ${CONFIG_FILE} ]; then
  echo "Configuration file not found: ${CONFIG_FILE}"
  exit 1
fi

/usr/local/bin/generate-config.py --input="${CONFIG_FILE}" > /etc/keepalived/keepalived.conf
cat ${CONFIG_FILE}
echo
echo "################################################"
/usr/sbin/keepalived -v
echo
echo "################################################"
cat /etc/keepalived/keepalived.conf
echo
echo "################################################"
exec /usr/sbin/keepalived $KEEPALIVED_OPTS

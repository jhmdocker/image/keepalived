## Health check for an external service.
## i.e: a web server that uses this container as a sidecar.
#/usr/bin/curl -sf http://127.0.0.1 || exit 1

#!/bin/sh -e

# Create /tmp/keepalived.stats
/bin/kill -USR2 $(cat /var/run/keepalived/keepalived.pid)

# Check for authentication errors
! cat /tmp/keepalived.stats | grep Failure | grep -vq 'Failure: 0'

# Custom health check for an external service that uses
# this container as a sidecar.
sh -e /usr/local/bin/custom-healthcheck.sh

#!/usr/bin/python3

import argparse
import yaml
import netifaces;
import sys

def parseArguments():
  parser = argparse.ArgumentParser(description="Generate a keepalived.conf based on a simplified YAML config file")
  parser.add_argument("-i","--input", required=True, dest="config_file", metavar="<CONFIG_FILE>", help="Configuration file in a YAML format")
  return parser.parse_args()

## Global config
def globalSection():
  print("""
global_defs {
  enable_script_security
  max_auto_priority 99
}""")

## Creat a script section for health checks
def scriptSection():
  print("""
vrrp_script custom_healthcheck {
  script "/usr/local/bin/healthcheck.sh"
  interval 2
  timeout 1
  rise 5
  fall 5
}""")

## Generate an instance section for each virtual IP
def instanceSection(config, netInfo):
  for c in config['instances']:
    initial_state = 'BACKUP'
    priority=100
    # Elege o node atual como master
    if netInfo.ip_address == c['ip_address']:
      initial_state = 'MASTER'
      priority+=priority

    data = {
      'vrrp_name': c['virtual_ip'].replace('.','_'),
      'state': initial_state,
      'iface': netInfo.ifname,
      'hostname' : netInfo.hostname,
      'ip_address' : netInfo.ip_address,
      'vrouter_id' : c['vrouter_id'],
      'priority': priority,
      'virtual_ip' : c['virtual_ip'],
      'auth_pass' : config['authentication']['password']
    }

    print("""
vrrp_instance VRRP_%(vrrp_name)s {
  state %(state)s
  interface %(iface)s
  # ip of %(hostname)s
  mcast_src_ip %(ip_address)s
  virtual_router_id %(vrouter_id)s
  priority %(priority)d

  promote_secondaries

  virtual_ipaddress {
    %(virtual_ip)s
  }

  track_script {
    custom_healthcheck
  }

  authentication {
    auth_type PASS
    auth_pass %(auth_pass)s
  }

}""" % data)

class NetworkInfo:
  def __init__(self, hostname, ifname, ip_address):
    self.hostname = hostname
    self.ifname = ifname
    self.ip_address = ip_address

## Find the network iface and IP for the node where this is running
def getNetworkInfo(config):
  for c in config['instances']:
    for ifname in netifaces.interfaces():
      addresses = netifaces.ifaddresses(ifname)
      if netifaces.AF_INET in addresses:
        for ipv4_address in addresses[netifaces.AF_INET]:
          if ipv4_address['addr'] == c['ip_address']:
            return NetworkInfo(c['hostname'], ifname, c['ip_address'])

## Main function
if __name__ == '__main__':
 
  args = parseArguments()

  with open(args.config_file) as yamlFile:
    config = yaml.safe_load(yamlFile)

    netInfo = getNetworkInfo(config)
    if netInfo is None:
      sys.exit("Unable to determine the ip address of this node based on YAML configuration file")
  
    # create global config section
    #globalSection()
    
    # create healthcheck section
    scriptSection()
    
    # create vrrp instance sections
    instanceSection(config, netInfo)

ALPINE_VERSION=3.20
KEEPALIVED_VERSION=2.2.8-r0
IMG=registry.gitlab.com/jhmdocker/image/keepalived

build:
	docker build $(BUILD_OPTS) -t $(IMG) \
		--build-arg ALPINE_VERSION=$(ALPINE_VERSION) \
		--build-arg KEEPALIVED_VERSION=$(KEEPALIVED_VERSION) \
		.

push: build
	docker tag $(IMG) $(IMG):$(KEEPALIVED_VERSION)
	docker push $(IMG)
	docker push $(IMG):$(KEEPALIVED_VERSION)

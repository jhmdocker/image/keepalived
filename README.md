# Keepalived

A keepalived instalation ready to be used in Kubernetes clusters. Configuration upon `/etc/keepalived/keepalived.conf` is dynamically generated at entrypoing using a simplified Yaml file `/etc/keepalived/config.yaml`.

Take a look at `test/config.yaml` for an full example configuration file. It shows how to run 3 instances of keepalived, each one with its own private IP. Three virtual IPs are balanced among the instances, creating a full load balance among all of them. If one instance is taken down, one of the other remaining instances will assume control of that virtual IP, handing it over when the failed instance is brought back online.

# Examples

Create a docker network for this example:
```
docker network create --subnet 10.252.252.0/24 net252
```

Create two containers:
```
docker run --rm -it --name node02 --privileged --network net252 -v (pwd)/example/config.yaml:/etc/keepalived/config.yaml registry.gitlab.com/jhmdocker/image/keepalived

docker run --rm -it --name node03 --privileged --network net252 -v (pwd)/example/config.yaml:/etc/keepalived/config.yaml registry.gitlab.com/jhmdocker/image/keepalived
```

Ping test the virtual IPs
```
ping 10.252.252.202
...
ping 10.252.252.203
```

Kill one of the containers and observe that ping continues to respond normally on both IPs.
